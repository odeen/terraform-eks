variable "cluster-name" {
  default = "terraform-eks-devops"
  type    = string
}

variable "aws_region" {
  default = "eu-central-1"
  type    = string
}

variable "aws_access_key" {
  default = ""
  type    = string
}

variable "aws_secret_key" {
  default = ""
  type    = string
}

variable "rds_password" {
  default = "root1234"
}

variable "rds_username" {
  default = "root"
  type    = string
}

variable "rds_db" {
  default = "wordpress"
  type    = string
}
