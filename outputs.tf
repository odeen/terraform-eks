# outputs
locals {
  kubeconfig = <<KUBECONFIG


apiVersion: v1
clusters:
- cluster:
    server: ${aws_eks_cluster.devops.endpoint}
    certificate-authority-data: ${aws_eks_cluster.devops.certificate_authority[0].data}
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: aws
  name: aws
current-context: aws
kind: Config
preferences: {}
users:
- name: aws
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1alpha1
      command: aws
      args:
        - "--region"
        - "${var.aws_region}"
        - "eks"
        - "get-token"
        - "--cluster-name"
        - "${var.cluster-name}"
KUBECONFIG

}

output "kubeconfig" {
  value = local.kubeconfig
}

# Join configuration

locals {
  config-map-aws-auth = <<CONFIGMAPAWSAUTH


apiVersion: v1
kind: ConfigMap
metadata:
  name: aws-auth
  namespace: kube-system
data:
  mapRoles: |
    - rolearn: ${aws_iam_role.devops-node.arn}
      username: system:node:{{EC2PrivateDNSName}}
      groups:
        - system:bootstrappers
        - system:nodes
CONFIGMAPAWSAUTH

}

output "config-map-aws-auth" {
  value = local.config-map-aws-auth
}

output "rds-postgres" {
  value = aws_db_instance.wordpress-postgres.address
}

output "rds" {
  value = aws_db_instance.wordpress-mariadb.address
}
