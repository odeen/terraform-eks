### Install

Make sure you have configured you AWS account: aws configure

Run

```console
$ terraform apply -auto-approve
$ terraform output kubeconfig > ~/.kube/config
$ terraform output config-map-aws-auth | kubectl apply -f -
$ terraform output rds
```


### ToDo

. add backend for terraform: s3 + dynamodb for locking
. add KMS for RDS
. add cloudwatch
. add db backups
