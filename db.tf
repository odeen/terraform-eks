resource "aws_db_subnet_group" "wordpress-db" {
  name        = "wordpress-db"
  description = "Database subnet group for Wordpress"
  subnet_ids  = module.vpc.private_subnets

  tags = {
    "Name" = "Wordpress-DB-Subnet"
  }
}

resource "aws_security_group" "wordpress-db" {
  name        = "Wordpress-DB"
  description = "Security Group for Wordpress AWS RDS database"
  vpc_id      = module.vpc.vpc_id
  tags = {
    "Name" = "Wordpress-DB-SG"
  }
}

resource "aws_security_group_rule" "database-cluster-ingress" {
  description = "Allow access to postgres from cluster"
  type        = "ingress"
  from_port   = 5432
  to_port     = 5432
  protocol    = "tcp"

  security_group_id        = aws_security_group.wordpress-db.id
  source_security_group_id = aws_security_group.devops-node.id
}

resource "aws_security_group_rule" "database-cluster-egress" {
  description = "Allow access to postgres from cluster"
  type        = "egress"
  from_port   = 5432
  to_port     = 5432
  protocol    = "tcp"

  security_group_id        = aws_security_group.wordpress-db.id
  source_security_group_id = aws_security_group.devops-node.id
}

resource "aws_db_instance" "wordpress-postgres" {
  allocated_storage       = 100 # 100 GB of storage, gives us more IOPS than a lower number
  max_allocated_storage   = 200 # Enable Storage Autoscaling
  engine                  = "postgres"
  instance_class          = "db.t2.small" # use micro if you want to use the free tier
  identifier              = "wordpress-db"
  name                    = var.rds_db
  username                = var.rds_username # username
  password                = var.rds_password # password
  db_subnet_group_name    = aws_db_subnet_group.wordpress-db.name
  multi_az                = "true" # set to true to have high availability: 2 instances synchronized with each other
  vpc_security_group_ids  = [aws_security_group.wordpress-db.id]
  storage_type            = "gp2"
  backup_retention_period = 30   # how long you’re going to keep your backups
  skip_final_snapshot     = true # skip final snapshot when doing terraform destroy
  tags = {
    Name = "wordpress-postgres"
  }
}

resource "aws_ssm_parameter" "secret" {
  name        = "/database/password/master"
  description = "Database password"
  type        = "SecureString"
  value       = var.rds_password
}


# resource "aws_s3_bucket" "wordpress-backups" {
#   bucket = "wordpress-db-backups"
#   acl    = "private"
#
#   server_side_encryption_configuration {
#     rule {
#       apply_server_side_encryption_by_default {
#         sse_algorithm = "aws:kms"
#       }
#     }
#   }
#
#   versioning {
#     enabled = true
#   }
#
# }
