data "http" "workstation-external-ip" {
  # url = "https://ifconfig.co/"
  url = "http://ipv4.icanhazip.com"
}

locals {
  workstation-external-cidr = "${chomp(data.http.workstation-external-ip.body)}/32"
}
