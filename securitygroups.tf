resource "aws_security_group" "devops-cluster" {
  name        = "eks-cluster"
  description = "Cluster communication with worker nodes"
  vpc_id      = module.vpc.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "terraform-eks-devops"
  }
}

resource "aws_security_group_rule" "devops-cluster-ingress-node-https" {
  description              = "Allow pods to communicate with the cluster API Server"
  from_port                = 443
  protocol                 = "tcp"
  security_group_id        = aws_security_group.devops-cluster.id
  source_security_group_id = aws_security_group.devops-node.id
  to_port                  = 443
  type                     = "ingress"
}

resource "aws_security_group_rule" "devops-cluster-ingress-workstation-https" {
  // @toDo check this, remove this, otherwise it will work only with my user
  cidr_blocks       = [local.workstation-external-cidr]
  description       = "Allow workstation to communicate with the cluster API Server"
  from_port         = 443
  protocol          = "tcp"
  security_group_id = aws_security_group.devops-cluster.id
  to_port           = 443
  type              = "ingress"
}
