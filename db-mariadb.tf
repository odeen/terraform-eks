resource "aws_db_subnet_group" "wordpress-mariadb" {
  name        = "wordpress-mariadb"
  description = "Database subnet group for Wordpress"
  subnet_ids  = module.vpc.private_subnets

  tags = {
    "Name" = "Wordpress-MariaDB-Subnet"
  }
}

resource "aws_security_group" "wordpress-mariadb" {
  name        = "Wordpress-MariaDB"
  description = "Security Group for Wordpress AWS RDS database"
  vpc_id      = module.vpc.vpc_id
  tags = {
    "Name" = "Wordpress-MariaDB-SG"
  }
}

resource "aws_security_group_rule" "mariadb-cluster-ingress" {
  description = "Allow access to mariadb from cluster"
  type        = "ingress"
  from_port   = 3306
  to_port     = 3306
  protocol    = "tcp"

  security_group_id        = aws_security_group.wordpress-mariadb.id
  source_security_group_id = aws_security_group.devops-node.id
}

resource "aws_security_group_rule" "mariadb-cluster-egress" {
  description = "Allow access to mariadb from cluster"
  type        = "egress"
  from_port   = 3306
  to_port     = 3306
  protocol    = "tcp"

  security_group_id        = aws_security_group.wordpress-mariadb.id
  source_security_group_id = aws_security_group.devops-node.id
}

resource "aws_db_instance" "wordpress-mariadb" {
  allocated_storage       = 100 # 100 GB of storage, gives us more IOPS than a lower number
  max_allocated_storage   = 200 # Enable Storage Autoscaling
  engine                  = "mariadb"
  instance_class          = "db.t2.small" # use micro if you want to use the free tier
  identifier              = "wordpress-mariadb"
  name                    = var.rds_db
  username                = var.rds_username # username
  password                = var.rds_password # password
  db_subnet_group_name    = aws_db_subnet_group.wordpress-mariadb.name
  multi_az                = "false" # set to true to have high availability: 2 instances synchronized with each other
  vpc_security_group_ids  = [aws_security_group.wordpress-mariadb.id]
  storage_type            = "gp2"
  backup_retention_period = 30   # how long you’re going to keep your backups
  skip_final_snapshot     = true # skip final snapshot when doing terraform destroy
  tags = {
    Name = "wordpress-mariadb"
  }
}
